Imports System.Xml
Imports System.IO
Imports ntb_FuncLib
Imports Obi2NITF
Imports System.Text
Imports System.Text.RegularExpressions

Public Class Convert

    Private Shared CR As String = Chr(&HD)

    Public Shared Function Obi2NITFByRef(ByVal strFileName As String, ByRef strNitfPath As String, ByRef strXMLTempPath As String, ByRef dtTimeStamp As DateTime, ByRef errImport As Exception, ByVal errorNitfPath As String, ByVal NITFVersion As Integer, ByVal iptc As Integer) As String
        Const FOOTER As String = "</body.content>" & vbCrLf & "</body>" & vbCrLf & "</nitf>"
        Dim strDoc As String
        Dim sbDoc As StringBuilder = New StringBuilder()
        Dim strType As String
        Dim strFileShort As String = Path.GetFileNameWithoutExtension(strFileName)

        'Load files
        Try
            strDoc = LogFile.ReadFile(strFileName)
        Catch ex As Exception
            errImport = ex
            Return "error"
        End Try

        dtTimeStamp = File.GetLastWriteTime(strFileName)
        Dim strOutFile As String = strNitfPath & "\" & strFileShort.Replace("$", "-") & Format(dtTimeStamp, "_yyyy-MM-dd_HH-mm") & ".xml"

        ' Evaluate Filetypes
        If strFileShort.IndexOf("kortbors") > -1 Then
            strType = "obi-kortb�rs"
        ElseIf strFileShort.IndexOf("kortfond") > -1 Then
            strType = "obi-kortfond"
        ElseIf strFileShort.IndexOf("topp") > -1 Then
            strType = "obi-topp-bunn"
        ElseIf strFileShort.IndexOf("fond") > -1 Then
            strType = "obi-fondsliste"
        ElseIf strFileShort.IndexOf("rundbors") > -1 Then
            strType = "obi-rundb�rs"
        Else
            Return "skip"
        End If

        sbDoc.Append(Regex.Replace(strDoc, "(\x20){2,}", " "))
        sbDoc.Append(vbCrLf)

        ' Replace Special Chars:
        sbDoc.Replace("&", "&amp;")
        sbDoc.Replace("<", "&lt;")
        sbDoc.Replace(Chr(140), "�")
        sbDoc.Replace(Chr(138), "�")

        sbDoc.Replace(Chr(154), "�")
        sbDoc.Replace(Chr(129), "�")
        sbDoc.Replace(Chr(133), "�")

        sbDoc.Replace("�", "�")
        sbDoc.Replace("�", "�")
        sbDoc.Replace("�", "�")
        sbDoc.Replace("�", "�")

        ' Make XML from XTG tags
        sbDoc.Replace("@tittel:", "<body.head>" & vbCrLf & "<hedline>" & vbCrLf & "<hl1>")

        sbDoc.Replace(CR & "@txt:", "</hl1>" & vbCrLf & "</hedline>" & vbCrLf & _
            "</body.head>" & vbCrLf & _
            "<body.content>" & vbCrLf & "<p>")

        ' Ekstra run because of missing ":" in XTG-tag
        sbDoc.Replace(CR & "@txt", "</hl1>" & vbCrLf & "</hedline>" & vbCrLf & _
            "</body.head>" & vbCrLf & _
            "<body.content>" & vbCrLf & "<p>")

        sbDoc.Replace("</hedline>", "</hedline>" & vbCrLf & "<distributor>" & vbCrLf & "<org>NTB</org>" & vbCrLf & "</distributor>")

        sbDoc.Replace(CR & CR & "[", "</p>" & vbCrLf & "[")

        strDoc = sbDoc.ToString

        Select Case strType
            Case "obi-kortb�rs"
                'Mellomtittel
                PairReplace(sbDoc, "[obt]" & CR, CR, "<p class=""table-code"">[obt]</p>" & vbCrLf & "<hl2 class=""notext"">", "</hl2>" & vbCrLf, 1, -1)
                PairReplace(sbDoc, "[obut]" & CR, CR, "<p class=""table-code"">[obut]</p>" & vbCrLf & "<p class=""notext"">", "</p>" & vbCrLf, 1, -1)

                ' TopTable heads
                sbDoc.Replace("[obkk1]{tn}", "<p class=""table-code"">[obkk1]</p>" & vbCrLf & "<table>")
                sbDoc.Replace("[obkk2]{tn}", "<p class=""table-code"">[obkk2]</p>" & vbCrLf & "<table>")

                ' TopTable footer
                sbDoc.Replace("[xobk1]{tn}", "</table>" & vbCrLf & "<p class=""table-code"">[xobk1]</p>")
                sbDoc.Replace("[xobk2]", "</table>" & vbCrLf & "<p class=""table-code"">[xobk2]</p>")

                'BUGFIX: Blank tables
                sbDoc.Replace("[obkk3]{tn}" & CR & "[xobk]", "")

                ' Table heads
                sbDoc.Replace("[obkk3]{tn}", "<p class=""table-code"">[obkk3]</p>" & vbCrLf & "<table>")
                sbDoc.Replace("[obkk51]{tn}", "<p class=""table-code"">[obkk51]</p>" & vbCrLf & "<table>")

                ' Table footer
                sbDoc.Replace("[xobk]", "</table>" & vbCrLf & "<p class=""table-code"">[xobk]</p>")

                ' Exception Table row ends
                sbDoc.Replace("{tr}" & CR, "</td></tr>" & vbCrLf)

            Case "obi-kortfond"
                'Mellomtittel
                sbDoc.Replace("[TE]{tn}" & CR, "<p class=""table-code"">[TE]</p>" & vbCrLf & "<hl2 class=""notext"">")

                ' TopTable heads
                sbDoc.Replace("[FND]{tn}", "</p>" & vbCrLf & "<p class=""table-code"">[FND]</p>" & vbCrLf & "<table>")
                ' TopTable footer
                sbDoc.Replace("[XFND]{tn}", "</table>" & vbCrLf & "<p class=""table-code"">[XFND]</p>")

                ' Table heads
                sbDoc.Replace("[XTE]{tn}", "</hl2>" & vbCrLf & "<p class=""table-code"">[XTE]</p>" & vbCrLf & "<table>")

                ' Table footer
                sbDoc.Replace("{et}", "</table>")

            Case "obi-topp-bunn"
                ' Top
                sbDoc.Replace("[TOP]{tn}" & CR, "</p>")

                'Mellomtittel
                sbDoc.Replace("[TOP5]{tn}" & CR, "<hl2>")
                sbDoc.Replace(CR & "[XTOP5]{tn}", "</hl2>")

                sbDoc.Replace("[BUN5]{tn}" & CR, "<hl2>")
                sbDoc.Replace(CR & "[XBUN5]{tn}", "</hl2>")

                ' TopTable heads
                sbDoc.Replace("[TOP0]{tn}", "<table>")

                ' Table heads
                sbDoc.Replace("[TABE5]{tn}", "</table>" & vbCrLf & "<table>")

                ' Table footer
                sbDoc.Replace("{et}", "</table>")

                ' Remove unused tags
                sbDoc.Replace("@fond_subtitle:", "")
                sbDoc.Replace("�", "--")

            Case "obi-fondsliste"
                'Mellomtittel

                ' TopTable heads
                sbDoc.Replace("[FOND]{tn}", "</p>" & vbCrLf & "<p class=""table-code"">[FOND]</p>" & vbCrLf & "<table>")

                ' TopTable footer
                sbDoc.Replace("[XFOND]{tn}", "</table>" & vbCrLf & "<p class=""table-code"">[XFOND]</p>")

                ' Table heads
                sbDoc.Replace("[TAB]{tn}", "<p class=""table-code"">[TAB]</p>" & vbCrLf & "<table>")

                ' Table footer
                sbDoc.Replace("{et}", "</table>")

                PairReplace(sbDoc, "[TE]{tn}" & CR, CR & "[XTE]{tn}", "<p class=""table-code"">[TE]</p>" & vbCrLf & "<hl2 class=""notext"">", "</hl2>" & vbCrLf & "<p class=""table-code"">[XTE]</p>" & vbCrLf, 1, -1)

            Case "obi-rundb�rs"
                sbDoc.Replace(CR & CR, vbCrLf & "</p><p>" & vbCrLf)
                sbDoc.Append("</p>" & vbCrLf)
        End Select

        ' Common table codes for all files
        ' <TR> stop
        sbDoc.Replace("{tr}{tn}" & CR, "</td></tr>" & vbCrLf)
        sbDoc.Replace("{tc}{tn}" & CR, "</td></tr>" & vbCrLf)

        ' <TR> start
        sbDoc.Replace("{sl}", "<tr><td>")
        sbDoc.Replace(vbCrLf & "{tr}", vbCrLf & "<tr><td>")
        sbDoc.Replace(CR & "{tr}", vbCrLf & "<tr><td>")

        ' <TD>-breaks
        sbDoc.Replace("{tr}{tn}", "</td><td align='right'>")
        sbDoc.Replace("{tl}{sp}", "</td><td align='left'>")
        sbDoc.Replace("{tr}", "</td><td align='right'>")
        sbDoc.Replace("{tc}", "</td><td align='center'>")

        sbDoc.Replace("{eop}", "</p>" & vbCrLf & "<p>")

        PairReplace(sbDoc, "Kilde: ", vbCrLf, "<p>Kilde: ", "</p>" & vbCrLf, 1, 1)

        ' Add NITF header and footer
        Try
            If NITFVersion = 32 Then
                strDoc = MakeNitf32Header(strType, strOutFile, 0, dtTimeStamp, iptc) & sbDoc.ToString & FOOTER
            Else
                strDoc = MakeNitfHeader(strType, strOutFile, 0, dtTimeStamp) & sbDoc.ToString & FOOTER
            End If
        Catch ex As Exception
            errImport = ex
            Return "error"
        End Try

        'Save file
        Try
            LogFile.WriteFile(strOutFile, strDoc, False)
        Catch ex As Exception
            errImport = ex
            Return "error"
        End Try

        Return "ok"
    End Function

    Private Shared Sub PairReplace(ByRef sbDoc As StringBuilder, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, ByVal strReplace2 As String, ByVal intStart As Integer, ByVal intCount As Integer)
        ' Overload for StringBuilder Type strings
        Dim i As Integer
        Dim lpStart As Integer
        Dim lpSlutt As Integer
        Dim intToCount As Integer
        Dim strDoc As String

        If strFind = "" Or strFind2 = "" Then Exit Sub

        If intCount < 0 Then
            intToCount = 100000
        Else
            intToCount = intCount
        End If

        lpStart = intStart - 1
        For i = 1 To intToCount
            'strDoc = sbDoc.ToString

            lpStart = sbDoc.ToString.IndexOf(strFind, lpStart)
            If lpStart = -1 Then Exit For

            lpSlutt = sbDoc.ToString.IndexOf(strFind2, lpStart + Len(strFind) + 1)
            If lpSlutt = -1 Then Exit For

#If Debug Then
            'For debuging
            Dim strDoc1, strDoc2, strDoc3 As String
            strDoc1 = sbDoc.ToString.Substring(0, lpStart)
            strDoc2 = sbDoc.ToString.Substring(lpStart + Len(strFind), lpSlutt - lpStart - Len(strFind))
            strDoc3 = sbDoc.ToString.Substring(lpSlutt + Len(strFind2))
#End If
            ' Remove old and insert new start tag
            sbDoc.Remove(lpStart, Len(strFind))
            sbDoc.Insert(lpStart, strReplace)

            ' Calculate new end tag index
            lpSlutt = lpSlutt - Len(strFind) + Len(strReplace)

            ' Remove old and insert new end tag
            sbDoc.Remove(lpSlutt, Len(strFind2))
            sbDoc.Insert(lpSlutt, strReplace2)

            lpStart = lpSlutt + Len(strReplace)
        Next
    End Sub

    Private Shared Function PairReplace(ByVal strDoc As String, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, ByVal strReplace2 As String, ByVal intStart As Integer, ByVal intCount As Integer) As String
        ' Overload for normal strings
        Dim i As Integer
        Dim lpStart As Integer
        Dim lpSlutt As Integer
        Dim intToCount As Integer

        If strFind = "" Or strFind2 = "" Then Exit Function

        If intCount < 0 Then
            intToCount = 100000
        Else
            intToCount = intCount
        End If

        lpStart = intStart - 1
        For i = 1 To intToCount

            lpStart = strDoc.IndexOf(strFind, lpStart)
            If lpStart = -1 Then Exit For

            lpSlutt = strDoc.IndexOf(strFind2, lpStart + Len(strFind) + 1)
            If lpSlutt = -1 Then Exit For

#If Debug Then
            'For debuging
            Dim strDoc1, strDoc2, strDoc3 As String
            strDoc1 = strDoc.Substring(0, lpStart)
            strDoc2 = strDoc.Substring(lpStart + Len(strFind), lpSlutt - lpStart - Len(strFind))
            strDoc3 = strDoc.Substring(lpSlutt + Len(strFind2))
#End If

            strDoc = strDoc.Substring(0, lpStart) _
                & strReplace _
                & strDoc.Substring(lpStart + Len(strFind), lpSlutt - lpStart - Len(strFind)) _
                & strReplace2 _
                & strDoc.Substring(lpSlutt + Len(strFind2))
            lpStart = lpSlutt
        Next
        Return strDoc
    End Function

    Private Shared Function BulkReplace(ByVal strDoc As String, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, ByVal intStart As Integer, ByVal intCount As Integer) As String
        ' NB! Funksjonen b�r skrives om med bruk av strDoc.IndexOf og strDoc.Substring 
        ' i stedet for de gamle VB InStr() og Mid() funksjonene
        ' Er ikke testet for .NET enn�!
        Dim i As Long
        Dim lpStart As Integer
        Dim lpSlutt As Integer
        Dim intToCount As Long

        If strFind = "" Or strFind2 = "" Then Exit Function

        If intCount < 0 Then
            intToCount = 10000
        Else
            intToCount = intCount
        End If

        lpStart = intStart
        For i = 1 To intToCount
            lpStart = InStr(lpStart, strDoc, strFind)
            If lpStart = 0 Then Exit For
            lpSlutt = InStr(lpStart + Len(strFind), strDoc, strFind2)
            If lpSlutt = 0 Then Exit For
            strDoc = Mid(strDoc, 1, lpStart - 1) & strReplace & Mid(strDoc, lpSlutt + Len(strFind2))
        Next
        Return strDoc
    End Function

    Private Shared Function MakeNitfHeader(ByVal strType As String, ByVal strFileName As String, ByVal intDocLength As Integer, ByVal dtTimeStamp As DateTime) As String
        Dim sbDoc As StringBuilder = New StringBuilder()
        Dim strFileShort As String = Path.GetFileName(strFileName)

        Dim strNtbId As String = Format(dtTimeStamp, "yyyyMMdd_HHmm_") & strType.Replace("-", "")

        sbDoc.Append("<?xml version=""1.0"" encoding=""iso-8859-1""?>" & vbCrLf)

#If Debug Then
        sbDoc.Append("<?xml-stylesheet href=""ntb_sound.xsl"" type=""text/xsl""?>")
#End If

        sbDoc.Append("<nitf version=""-//IPTC-NAA//DTD NITF-XML 2.5//EN"" change.date=""9 august 2000"" change.time=""1900"" baselang=""no-NO"">" & vbCrLf)
        sbDoc.Append("<head>" & vbCrLf)
        sbDoc.Append("<title>" & strType & "</title>" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-folder"" content=""Ut-Satellitt"" />" & vbCrLf)
        'sbDoc.Append("<meta name=""ntb-date"" content=""05.09.2002 15:18"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-date"" content=""" & Format(dtTimeStamp, "dd.MM.yyyy HH:mm") & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-filename"" content=""" & strFileShort & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-distribusjonsKode"" content=""ALL"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-kanal"" content=""A"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-meldingsSign"" content=""valuta"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-meldingsType"" content=""Fip-melding"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-id"" content=""" & strNtbId & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-meldingsVersjon"" content=""0"" />" & vbCrLf)
        sbDoc.Append("<docdata>" & vbCrLf)
        sbDoc.Append("<evloc county-dist=""Riksnyheter;"">Norge;</evloc>" & vbCrLf)
        sbDoc.Append("<doc-id regsrc=""NTB"" id-string=""" & strNtbId & """ />" & vbCrLf)
        sbDoc.Append("<urgency ed-urg=""5"" />" & vbCrLf)
        sbDoc.Append("<date.issue norm=""" & Format(dtTimeStamp, "yyyyMMdd") & """ />" & vbCrLf)
        sbDoc.Append("<ed-msg info="""" />" & vbCrLf)
        sbDoc.Append("<du-key version=""1"" key=""" & strType & """ />" & vbCrLf)
        sbDoc.Append("<doc.copyright year=""" & Today.Year & """ holder=""NTB"" />" & vbCrLf)
        sbDoc.Append("<key-list>" & vbCrLf)
        sbDoc.Append("<keyword key=""" & strType & """ />" & vbCrLf)
        sbDoc.Append("</key-list>" & vbCrLf)
        sbDoc.Append("</docdata>" & vbCrLf)
        'sbDoc.Append("<pubdata date.publication=""20020905T151853"" item-length=""" & intDocLength & """ unit-of-measure=""character"" />" & vbCrLf)
        sbDoc.Append("<pubdata date.publication=""" & Format(dtTimeStamp, "yyyyMMddTHHmmss") & """ item-length=""" & intDocLength & """ unit-of-measure=""character"" />" & vbCrLf)
        sbDoc.Append("<revision-history name=""" & strType & """ />" & vbCrLf)
        sbDoc.Append("<tobject tobject.type=""Innenriks"">" & vbCrLf)
        sbDoc.Append("<tobject.property tobject.property.type=""Fulltekstmeldinger"" />" & vbCrLf)
        sbDoc.Append("<tobject.subject tobject.subject.refnum=""04000000"" tobject.subject.code=""OKO"" tobject.subject.type=""�konomi og n�ringsliv"" tobject.subject.matter=""Bank, finans og marked;"" />" & vbCrLf)
        sbDoc.Append("</tobject>" & vbCrLf)
        sbDoc.Append("</head>" & vbCrLf)
        sbDoc.Append("<body>" & vbCrLf)

        Return sbDoc.ToString

    End Function

    Private Shared Function MakeNitf32Header(ByVal strType As String, ByVal strFileName As String, ByVal intDocLength As Integer, ByVal dtTimeStamp As DateTime, ByVal iptc As Integer) As String
        Dim sbDoc As StringBuilder = New StringBuilder()
        Dim strFileShort As String = Path.GetFileName(strFileName)

        Dim strNtbId As String = Format(dtTimeStamp, "yyyyMMdd_HHmm_") & strType.Replace("-", "")

        sbDoc.Append("<?xml version=""1.0"" encoding=""iso-8859-1""?>" & vbCrLf)
        'sbDoc.Append("<!DOCTYPE nitf SYSTEM ""nitf-3-2.dtd"">" & vbCrLf)

#If Debug Then
        'sbDoc.Append("<?xml-stylesheet href=""ntb_sound.xsl"" type=""text/xsl""?>")
#End If

        sbDoc.Append("<nitf version=""-//IPTC//DTD NITF 3.2//EN"" change.date=""October 10, 2003"" change.time=""19:30"" baselang=""no-NO"">" & vbCrLf)
        sbDoc.Append("<head>" & vbCrLf)
        sbDoc.Append("<title>" & strType & "</title>" & vbCrLf)

        sbDoc.Append("<meta name=""timestamp"" content=""" & Format(dtTimeStamp, "yyyy.MM.dd HH:mm:ss") & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""subject"" content=""" & strType & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""foldername"" content=""Ut-Satellitt"" />" & vbCrLf)
        sbDoc.Append("<meta name=""filename"" content=""" & strFileShort & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBTjeneste"" content=""Nyhetstjenesten"" />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBDistribusjonsKode"" content=""ALL"" />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBKanal"" content=""A"" />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBStikkord"" content=""" & strType & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBPrioritet"" content=""5"" />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBMeldingsSign"" content=""OBI"" />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBMeldingsType"" content=""Fip-melding"" />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBIPTCSequence"" content=""" & iptc & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBID"" content=""" & strNtbId & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-dato"" content=""" & Format(dtTimeStamp, "dd.MM.yyyy HH:mm") & """ />" & vbCrLf)

        sbDoc.Append("<tobject tobject.type=""Innenriks"">" & vbCrLf)
        sbDoc.Append("<tobject.property tobject.property.type=""Fulltekstmeldinger"" />" & vbCrLf)
        sbDoc.Append("<tobject.subject tobject.subject.code=""OKO"" tobject.subject.refnum=""04000000"" tobject.subject.type=""�konomi og n�ringsliv""/>" & vbCrLf)
        sbDoc.Append("<tobject.subject tobject.subject.code=""OKO"" tobject.subject.refnum=""04006000"" tobject.subject.matter=""Bank, finans og marked"" />" & vbCrLf)
        sbDoc.Append("</tobject>" & vbCrLf)

        sbDoc.Append("<docdata>" & vbCrLf)
        sbDoc.Append("<evloc state-prov=""Norge""/>" & vbCrLf)
        sbDoc.Append("<evloc state-prov=""Norge"" county-dist=""Riksnyheter""/>" & vbCrLf)
        sbDoc.Append("<doc-id regsrc=""NTB"" id-string=""" & strNtbId & """ />" & vbCrLf)
        sbDoc.Append("<urgency ed-urg=""5"" />" & vbCrLf)
        sbDoc.Append("<date.issue norm=""" & Format(dtTimeStamp, "yyyy-MM-ddTHH:mm:ss") & """ />" & vbCrLf)
        sbDoc.Append("<ed-msg info="""" />" & vbCrLf)
        sbDoc.Append("<du-key version=""1"" key=""" & strType & """ />" & vbCrLf)
        sbDoc.Append("<doc.copyright year=""" & Today.Year & """ holder=""NTB"" />" & vbCrLf)
        sbDoc.Append("<key-list>" & vbCrLf)
        sbDoc.Append("<keyword key=""" & strType & """ />" & vbCrLf)
        sbDoc.Append("</key-list>" & vbCrLf)
        sbDoc.Append("</docdata>" & vbCrLf)

        sbDoc.Append("<pubdata date.publication=""" & Format(dtTimeStamp, "yyyyMMddTHHmmss") & """ item-length=""" & intDocLength & """ unit-of-measure=""character"" />" & vbCrLf)
        sbDoc.Append("<revision-history name=""" & strType & """ />" & vbCrLf)

        sbDoc.Append("</head>" & vbCrLf)
        sbDoc.Append("<body>" & vbCrLf)

        Return sbDoc.ToString

    End Function


End Class

