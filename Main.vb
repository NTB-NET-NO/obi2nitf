Imports System
Imports System.ServiceProcess
Imports System.Diagnostics
Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports System.Xml
Imports ntb_FuncLib

#If DEBUG Then
Public Module SimpleService
#Else
Public Class SimpleService : Inherits ServiceBase
#End If

    Const SERVICE_NAME As String = "NTB_Obi2NITF"
    Const TEXT_STARTED As String = SERVICE_NAME & " Service Started"

    '*** Global variables:
    Private NitfPath As String
    Private errorPath As String
    Private errFilePath As String
    Private logFilePath As String

    '*** Module variables:
    Private importPath As String
    Private donePath As String
    Private blnDebugXml As Boolean

    Private NITFVersion As Integer

    Protected IPTC_Sequence As Integer
    Protected IPTC_SequenceFile As String
    Protected IPTC_Range As String

    'Public FSW1 As FileSystemWatcher
    Friend WithEvents FSW1 As System.IO.FileSystemWatcher

    'Win API function Sleep
    Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

#If Debug Then
    Public Sub Main()
        InitConfigValues()
        ImportAll()
    End Sub
#Else

    Public Shared Sub Main()
        ServiceBase.Run(New SimpleService())
    End Sub

    Public Sub New()
        MyBase.New()
        CanPauseAndContinue = True
        ServiceName = SERVICE_NAME
    End Sub

    Protected Overrides Sub OnStop()
        FSW1.EnableRaisingEvents = False
        Sleep(5000)

        LoadSequence(IPTC_SequenceFile, False)

        LogFile.WriteLog(logFilePath, SERVICE_NAME & " Service stopped")
        EventLog.WriteEntry(SERVICE_NAME & " Service stopped")
    End Sub

    Protected Overrides Sub OnShutDown()
        OnStop()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            FSW1 = New FileSystemWatcher()

            '*** Init global variables
            InitConfigValues()

            '*** Enable filesystem watcher for Filecreation interrupt
            FSW1.IncludeSubdirectories = False
            FSW1.Filter = "*.txt"
            FSW1.Path = importPath

            LogFile.WriteLog(logFilePath, TEXT_STARTED)
            EventLog.WriteEntry(TEXT_STARTED)

            StartJob()

        Catch err As Exception
            Dim strErr As String = "Error in Initialization, program aborted"
            LogFile.WriteErr(errFilePath, strErr, err)
            EventLog.WriteEntry(strErr)
            ' End Program
            End
        End Try

    End Sub
#End If

    Private Sub InitConfigValues()
        ' Reading Config values from "Notabene2NITF.exe.config" file

        importPath = AppSettings("importPath")
        donePath = AppSettings("donePath")
        NitfPath = AppSettings("NitfPath")

        errorPath = AppSettings("errorPath")

        logFilePath = AppSettings("logFilePath")
        errFilePath = AppSettings("errFilePath")
        blnDebugXml = (AppSettings("DebugXml") = "yes")

        NITFVersion = AppSettings("NITFVersion")

        IPTC_Range = AppSettings("IPTCRange")
        IPTC_SequenceFile = AppSettings("IPTCSequenceFile")

        IPTC_sequence = CInt(IPTC_Range.Split("-")(0))
        LoadSequence(IPTC_SequenceFile, True)

        Dim xslFilePath As String = AppSettings("xslFilePath")

        LogFile.MakePath(importPath)
        LogFile.MakePath(donePath)
        LogFile.MakePath(NitfPath)
        LogFile.MakePath(errorPath)
        LogFile.MakePath(logFilePath)
        LogFile.MakePath(errFilePath)
        'TransForm.LoadXmlFiles(xslFilePath)
    End Sub

    Private Sub LoadSequence(ByVal file As String, ByVal load As Boolean)
        Try
            If load Then
                IPTC_Sequence = LogFile.ReadFile(file).Trim()
                LogFile.WriteLog(logFilePath, "IPTC Sequence number loaded: " & IPTC_Sequence)
            Else
                LogFile.WriteFile(file, IPTC_Sequence, False)
                LogFile.WriteLog(logFilePath, "IPTC Sequence number saved: " & IPTC_Sequence)
            End If
        Catch ex As Exception
            LogFile.WriteErr(logFilePath, "Error accessing IPTC sequence number.", ex)
        End Try
    End Sub

    Private Function Get_Seq() As Integer

        'Check value
        If IPTC_Sequence > CInt(IPTC_Range.Split("-")(1)) Or _
           IPTC_Sequence < CInt(IPTC_Range.Split("-")(0)) Then
            IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
        End If

        'Return value
        Dim ret As Integer = IPTC_Sequence

        'Increment
        IPTC_Sequence += 1

        'Save
        LoadSequence(IPTC_SequenceFile, False)

        Return ret
    End Function

    Private Sub StartJob()
        ' Start service
        ' All files waiting in import folder will be imported:
        ImportAll()

        ' "Sub FSW1_Created" will now trigger on Filesystem event: Created
        ' and do all new files appearing from now on:
        FSW1.EnableRaisingEvents = True

    End Sub

    Sub FSW1_Created(ByVal sender As Object, ByVal ev As System.IO.FileSystemEventArgs) Handles FSW1.Created
        ' Disable events to prevent multi-threading conflicts!
        FSW1.EnableRaisingEvents = False

        Console.WriteLine(Now & ": New File Created: " & ev.Name)
        Sleep(2000)

        Try
            ImportOneFile(ev.FullPath)
        Catch e As Exception
            LogFile.WriteErr(errFilePath, "Error open SQL-server", e)
        End Try

        ' Enable events when all files have been imported
        FSW1.EnableRaisingEvents = True
        ' Extra importjob in case of more files arrived since last event
        ImportAll()

    End Sub

    Sub ImportAll()
        ' Process the list of files found in the directory
        Dim fileEntries As String() = Directory.GetFiles(importPath, "*.txt")

        If fileEntries.Length = 0 Then
            Return
        End If

        '*** Wait 3 seconds for all files to be written
        Sleep(3000)

        Dim strFileName As String
        For Each strFileName In fileEntries
            ImportOneFile(strFileName)
        Next

    End Sub

    Sub ImportOneFile(ByRef strFileName As String)
        ' Process the list of files found in the directory

        Dim strStatus As String
        Dim errImport As Exception
        Dim dtTimeStamp As DateTime

        ' Main job is done here:
        strStatus = Convert.Obi2NITFByRef(strFileName, NitfPath, "", dtTimeStamp, errImport, errorPath, NITFVersion, Get_Seq())

        ' Logging and handling of source files:
        Select Case strStatus
            Case "ok"
                'File converted
                LogFile.WriteLog(logFilePath, "File OK: " & strFileName)
                Try
                    File.Copy(strFileName, FuncLib.MakeSubDirDate(donePath & "\" & Path.GetFileName(strFileName), dtTimeStamp), True)
                    File.Delete(strFileName)
                Catch e As Exception
                    ' Error on moving  file
                    LogFile.WriteErr(errFilePath, "Error in Moving file: " & strFileName, e)
                End Try

            Case "error"
                ' File NOT converted because of error

                LogFile.WriteLog(logFilePath, "File failed: " & strFileName)
                LogFile.WriteErr(errFilePath, "ERROR importing:" & strFileName, errImport)
                Try
                    File.Copy(strFileName, errorPath & "\" & Path.GetFileName(strFileName))
                    File.Delete(strFileName)
                Catch e As Exception
                    ' Error on moving file
                    LogFile.WriteErr(errFilePath, "ERROR moving file:" & strFileName, e)
                End Try

            Case "skip"
                'Skip files

                Try
                    File.Delete(strFileName)
                    LogFile.WriteLog(logFilePath, "File skipped: " & strFileName)
                Catch e As Exception
                    ' Error on moving imported file
                    LogFile.WriteErr(errFilePath, "Error in Deleting file: ", e)
                End Try

            Case Else

        End Select

    End Sub

#If Debug Then
End Module
#Else
End Class
#End If
